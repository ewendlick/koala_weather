# koala_weather

This is a widget that display five day's worth of weather forecasts. It will attempt to retrieve geolocation information from the user's machine to return weather data, but it is also possible to retrieve weather forcasts by searching via city name.

![day mode screenshot](https://bitbucket.org/ewendlick/koala_weather/raw/master/daymode-screenshot.png) ![night mode screenshot](https://bitbucket.org/ewendlick/koala_weather/raw/master/nightmode-screenshot.png)

## Spec

The purpose of this test is for us to get a sense of how you would approach designing and implementing a JavaScript/CSS/HTML weather widget. Applicants are encouraged to write code for this test as for a production grade application.

There's more than one way to skin a cat, therefore feel free to list down your assumptions as you go along (e.g. in code comments).

Although not required, you can use any front-end framework you like. Vue.js will be looked upon favourably.

### Requirements

Based on the wireframe, create a “weather widget” app that:

1. Shows your location's current and future weather data.
2. Able to search a specific location weather data.
3. Has a dark / light mode.
4. Adheres to minimum AA accessibility guidelines.

Wireframes are intentionally left plain for maximum creativity! A delightful use of animations is a plus.

##### Widget

The widget should read the end user’s current location using `navigator.geolocation` (or the entered location), and retrieve the current weather conditions for that location using the [Open Weather Map API](https://openweathermap.org/api). Defaulted to the current time, users should be able to navigate between the days. (see image below)

![weather widget](https://bitbucket.org/ewendlick/koala_weather/raw/master/weather-widget.jpg)

### Deliverables

There is no strict time limit (unless agreed upon) but most people get back to us within a few days. Source code for the solution described above: share your repository, upload it as an accessible link or forward a zip file of the source code.


## Design notes and changelog

### 2020-08-29
* ✅ Install Axios, Vuex, jest (no router)
* ✅ Create app.vue. Search.vue. Weather.vue, NightmodeToggle.vue
* ✅ Fire API calls to 5 Day / 3 Hour Forecast
*  Vuex weather (set_weather_by_location, set_displayed_weather_by_location, load_weather(location, date)) - cached_weather[location][date]
* ✅ Prompt user for access to their GPS position. If GPS, fire API with that
* ✅ If GPS becomes available, first API with that
* ✅ Otherwise, pull something from their navigation and fire API with that(?)
* ✅ When user enters location into searchbox and presses enter or search, fire the API (to get GPS?)
* ✅ Create night mode and day mode toggle
*  Create night mode and day mode styles
* ❌ Install icon pack - *Did not do*: Didn't find one that I liked
* ✅ Display icon associated with weather
* ✅ Display temperature
*  Create layout
*  Fade on nav buttons coming in and out

* ✅ Reach) If an API call failed. Display a message
* Reach) Get user’s coordinates via IP address
* Reach) Get location from cookie

### 2020-08-30

* ✅ Fix styles on search input
* ✅ Move night mode toggle to upper right
* ✅ Improve night mode toggle styles
* ✅ Add text to nav buttons
* ✅ Change size of nav buttons
* ✅ Create all (20 svg/20 image) other icons
* ✅ Improve layout
* ✅ Display city name
* ✅ Decrease height
* ✅ Apply night mode styles to background
* ✅ Apply night mode styles to text in Weather.vue
* ✅ Apply night mode styles to icons
*  Apply night mode styles to loading spinner
*  Convert most CSS to BEM
* ✅ CSS variables into their own file
* ✅ Clean up README.md
* ✅ Koala background behind widget
* ✅ Remove unused icons
* ✅ Add icons to navigation
* ✅ Update location icon
* ❌ Move date content into utils - *Will not do*: Works better inside of component
* ❌ Add humidity info - *Will not merge to master*: A smaller icon meant a thinner stroke. The icon didn't look good.
* ✅ Change text in Nav to be day of the week
* ❌ Add more black outlines to page items - *Will not do*: Nothing looked good
* ✅ move .weather-icon style out
* ✅ Make first character in weather description string uppercase
* ✅ Hover styles on buttons
* ✅ Change color of icon in citySearch
* ❌ Double-check the retry button - *Will not do*: Removed, added instructions
* ❌ Double-check the isLoading - *Will not do*
* ✅ Confirm behavior when Geolocation is not given
*  Consider background images or something more exciting in the night mode toggle
* ✅ Accessibility
* ✅ Improve the click area of the "Go" button
* Debounce clicks
* Loading spinner for searches

* Reach) Transitions between pngs and svgs

# Assumptions

Several assumptions were made in designing this widget to reduce time required.

* It is assumed that no environments are required for this project. API base URL and API key strings were hardcoded instead of being placed in .env files
* It is assumed that fewer API calls are better. One API call to the 5 day forecast is used, and the current weather and daily temperatures at noon are filtered from the response.
* It is assumed that this widget does not need to be responsive.
* Only Celsius is used.
* Seeing weather at noon for days in the future would probably be useful to the user, so except for the current time of the search, all forecasts are for noon on that day.


# Known issues

* Results by city can be ambiguous. Searching for "Springfield" might not show you the weather forecast for the city where the Simpsons live.

* Clicking on those nav buttons really fast has resulted in incorrect data being displayed. 

Also, if there are any "why in the world did he do it this way?" questions I would be happy to answer them. I have ~~excuses~~ reasons for most design decisions, but the major one that influenced decisions in this project is "time constraints".

# Improvements

* It would be nice to have made this responsive.
* In terms of UI/UX, I think I should have shown the current weather and the 5 day forecast on the screen at the same time.
* SCSS variables should be loaded globally.
* Cache the user's last searched city in a cookie. Cache the API results.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).