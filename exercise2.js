// If Kevin is checking this please assure him that I didn't use spaces - I used quarter-width double-tabs.

const input = {
  "students": [
    {
      "name": "Lulu Gearside",
      "class": "art",
      "attended": 35
    },
    {
      "name": "Matthew Milham",
      "class": "art",
      "attended": 11
    },
    {
      "name": "Dany Dufner",
      "class": "biology",
      "attended": 12
    },
    {
      "name": "Jeremy Doyle",
      "class": "biology",
      "attended": 3
    },
    {
      "name": "Tim O'Connor",
      "class": "biology",
      "attended": 10
    },
    {
      "name": "L'Carpetron Dookmarriot",
      "class": "french",
      "attended": 12
    }
  ]
}

/*
Expected output:
{
  "art": {
    "total": 46,
    "average": 23,
  },
  "biology": {
    "total": 25,
    "average": 8,
  },
  "french": {
    "total": 12,
    "average": 12,
  },
}
*/

const generateAttendance = (students) => {
  const schoolClasses = {}
  const totalStudentCounts = {} // contains the additional data required for calculating averages

  students.forEach(student => {
    if (!schoolClasses[student.class]) {
      // Initializes values
      schoolClasses[student.class] = {
        total: student.attended,
        average: student.attended
      }
      totalStudentCounts[student.class] = {
        totalStudentCount: 1
      }
    } else {
      const totalStudentCount = totalStudentCounts[student.class].totalStudentCount + 1
      const total = schoolClasses[student.class].total + student.attended
      const average = Math.floor(total / totalStudentCount)

      schoolClasses[student.class] = {
        total,
        average
      }
      totalStudentCounts[student.class] = {
        totalStudentCount
      }
    }
  })

  return schoolClasses
}


const result = generateAttendance(input.students)
console.log(result)
