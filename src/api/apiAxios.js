import axios from 'axios'

const apiAxios = axios.create({
  baseURL: 'http://api.openweathermap.org/data/2.5',
  timeout: 30000,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  params: {
    appid: '65655131fc70c248a71d62c01f8f8b00',
    units: 'metric'
  }
})

export {
  apiAxios
}
