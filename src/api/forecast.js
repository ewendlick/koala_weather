import { apiAxios } from '@/api/apiAxios.js'

// Documentation: https://openweathermap.org/forecast5

const get5DayForecastByCityName = async (cityName) => {
  const params = {
    q: cityName
  }

  return format5DayResponse(await apiAxios.get('/forecast', { params: params }))
}

const get5DayForecastByCoordinates = async (latitude, longitude) => {
  const params = {
    lat: latitude,
    lon: longitude
  }

  return format5DayResponse(await apiAxios.get('/forecast', { params: params }))
}

const format5DayResponse = (forecastResult) => {
  const filteredResult = forecastResult.data.list.filter((threeHourWeatherSegment, index) => {
    if (index === 0) return true // Current weather
    // get weather at next day's noon by skipping at least midnight (0), 3am (1), 6am (2), 9am (3), noon (4) of current day
    if (index <= 4) return false

    const timestamp = new Date(threeHourWeatherSegment.dt * 1000)
    return timestamp.getHours() === 12
  })

  const days = filteredResult.map(day => {
    const timestamp = day.dt
    const temperature = Math.round(day.main.temp)
    const humidity = day.main.humidity
    const weatherType = day.weather[0].main
    const weatherDescription = day.weather[0].description

    return { timestamp, temperature, humidity, weatherType, weatherDescription }
  })

  return { days, cityName: forecastResult.data.city.name }
}

export {
  get5DayForecastByCityName,
  get5DayForecastByCoordinates
}
