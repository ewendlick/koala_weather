import Vue from 'vue'
import App from './App.vue'
import store from './store'
import VueAgile from 'vue-agile'

Vue.config.productionTip = false

Vue.use(VueAgile)

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
