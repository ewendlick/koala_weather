import Vue from 'vue'
import Vuex from 'vuex'

import * as forecastApi from '@/api/forecast.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isFetchingCoordinates: false,
    isApiLoading: false,
    geolocationErrorMessage: null,
    isNightMode: false,
    fiveDayForecast: [],
    cityName: '',
    coordinates: {
      lat: null,
      long: null
    }
  },
  mutations: {
    SET_IS_FETCHING_COORDINATES: (state, isFetchingCoordinates) => {
      state.isFetchingCoordinates = isFetchingCoordinates
    },
    SET_IS_API_LOADING: (state, isApiLoading) => {
      state.isApiLoading = isApiLoading
    },
    SET_GEOLOCATION_ERROR_MESSAGE: (state, geolocationErrorMessage) => {
      state.geolocationErrorMessage = geolocationErrorMessage
    },
    SET_NIGHT_MODE: (state, isNightMode) => {
      state.isNightMode = isNightMode
    },
    SET_FIVE_DAY_FORECAST: (state, fiveDayForecast) => {
      state.fiveDayForecast = fiveDayForecast
    },
    SET_CITY_NAME: (state, cityName) => {
      state.cityName = cityName
    },
    SET_COORDINATES: (state, { latitude, longitude }) => {
      state.coordinates = {
        latitude,
        longitude
      }
    }
  },
  actions: {
    setGeolocationCoordinatesToCurrentLocation ({ commit }) {
      commit('SET_GEOLOCATION_ERROR_MESSAGE', null)
      commit('SET_IS_FETCHING_COORDINATES', true)

      const successCallback = async (position) => {
        commit('SET_COORDINATES', { latitude: position.coords.latitude, longitude: position.coords.longitude })
        commit('SET_IS_FETCHING_COORDINATES', false)
        // TODO: Think about where this should go. This is the most convenient place, but maybe not the best
        commit('SET_IS_API_LOADING', true)
        try {
          const forecastApiResult = await forecastApi.get5DayForecastByCoordinates(position.coords.latitude, position.coords.longitude)
          commit('SET_FIVE_DAY_FORECAST', forecastApiResult.days)
          commit('SET_CITY_NAME', forecastApiResult.cityName)
          commit('SET_IS_API_LOADING', false)
        } catch (forecastApiError) {
          commit('SET_FIVE_DAY_FORECAST', [])
          commit('SET_CITY_NAME', null)
          commit('SET_IS_API_LOADING', false)
          console.warn('[Forecast API ERROR]', forecastApiError)
        }
      }
      const failureCallback = (geolocationError) => {
        commit('SET_GEOLOCATION_ERROR_MESSAGE', geolocationError.message || geolocationError) // Not displayed
        commit('SET_IS_FETCHING_COORDINATES', false)
        commit('SET_IS_API_LOADING', false)
        console.warn('[Geolocation ERROR]', geolocationError)
      }
      navigator.geolocation.getCurrentPosition(successCallback, failureCallback, { timeout: 7000, enableHighAccuracy: false, maximumAge: 60 * 60 * 1000 }) // Caches location for 1 hour
    }
  },
  modules: {
  }
})
